import logging
import os
import asyncio
import typing

import yaml
import aiohttp
from pathlib import Path

from SibylSystem import PsychoPass
from pyee import AsyncIOEventEmitter
from pyrogram import Client
from pyrogram.parser import parser

# Globals.
loop = asyncio.get_event_loop()
help_dict = dict()
apps = dict()
app_user_ids = dict()
log_peer_ids = []
statistics = {}
ee = AsyncIOEventEmitter(loop=loop)
# awaited task load measurements
loads = {
    1: 0.0,  # 1 minute load
    5: 0.0,  # 5 minute load
    15: 0.0,  # 15 minute load
    30: 0.0  # 30 minute load
}


# this code here exists because i can't be fucked
class Parser(parser.Parser):
    async def parse(self, text, mode):
        if mode == 'through':
            return text
        return await super().parse(text, mode)


# Dirty hack to get around python's global variable scope copying
# bulllshit. Thanks @TheKneesocks for this code!
class ObjectProxy:
    def __init__(self, thing=None):
        self.thing = thing

    def __getattr__(self, attr):
        return getattr(self.thing, attr)

    def __bool__(self):
        return bool(self.thing)

    def __repr__(self):
        return repr(self.thing)

    def __str__(self):
        return str(self.thing)

    def __iter__(self):
        yield iter(self.thing)

    def __call__(self, *args, **kwargs):
        return self.thing(*args, **kwargs)

    def get_thing(self):
        return self.thing

    def set_thing(self, thing):
        self.thing = thing


#################
# ShadowHawk main
#
with open('config.yaml') as c:
    config = yaml.safe_load(c)

sessions_path = Path("sessions")
sessions_test_path = sessions_path / "test"
sessions_path.mkdir(exist_ok=True)

for i, session_name in enumerate(config['config']['sessions']):
    testserver = False
    workdir = str(sessions_path)
    if session_name.startswith("test:"):
        sessions_test_path.mkdir(exist_ok=True)
        session_name = session_name[5:]
        testserver = True
        workdir = str(sessions_test_path)
    app = Client(session_name, api_id=config['telegram']['api_id'], api_hash=config['telegram']['api_hash'],
                 plugins={'root': os.path.join(__package__, 'plugins')}, parse_mode='html', workdir=workdir,
                 test_mode=testserver)
    app.parser = Parser(app)
    # Temporarily assign a sequential integer while we're starting up.
    apps[i] = app

sibyl_client: typing.Union[PsychoPass, None]

try:
    sibyl_client = PsychoPass(config['config']['sibyl_api'])
except BaseException as e:
    logging.warning("Failed to load sibyl due to {}".format(e))
    sibyl_client = None

slave = Client('ShadowHawk-slave', api_id=config['telegram']['api_id'], api_hash=config['telegram']['api_hash'],
               plugins={'root': os.path.join(__package__, 'slave-plugins')}, parse_mode='html',
               bot_token=config['telegram']['slave_bot_token'], workdir='sessions')
slave.parser = Parser(slave)
# Http client session for making various HTTP requests
session = aiohttp.ClientSession()
# Get the info on what the server supports
server_support = ObjectProxy()
# This is here to avoid circular includes.
from shadowhawk.utils.AdminCacheManager import AdminCacheManager

acm = AdminCacheManager()
###############

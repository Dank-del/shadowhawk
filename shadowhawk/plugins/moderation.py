from datetime import datetime
from enum import Flag, unique
import html, asyncio, calendar, humanize, Levenshtein
from typing import List
from dateparser import parse
from pyrogram import Client, filters
from pyrogram.types import Message, ChatPermissions, ChatMember
from pyrogram.errors.exceptions.bad_request_400 import UsernameNotOccupied, SecondsInvalid
from shadowhawk.utils import self_destruct, build_name_flags, get_entity, get_id_from_app
from shadowhawk.utils.Logging import log_errors, public_log_errors, log_chat
from shadowhawk.utils.Command import parse_command
from shadowhawk import config, help_dict, acm

mute_permissions = ChatPermissions(
    can_send_messages=False,
    can_send_media_messages=False,
    can_send_stickers=False,
    can_send_animations=False,
    can_send_games=False,
    can_use_inline_bots=False,
    can_add_web_page_previews=False,
    can_send_polls=False,
    can_change_info=False,
    can_invite_users=False,
    can_pin_messages=False,
)

@unique
class UserPermissions(Flag):
	# Permissions 
	CAN_USE_ANONYMOUS         = 0x00001 # Can remain anonymous
	CAN_MANAGE_CHAT           = 0x00002 # Can view recent actions, stats, etcc.
	CAN_POST_MESSAGES         = 0x00004 # Can send messages
	CAN_EDIT_MESSAGES         = 0x00008 # Can edit messages
	CAN_DELETE_MESSAGES       = 0x00010 # Can delete messages
	CAN_RESTRICT_MEMBERS      = 0x00020 # Can ban/kick/mute chat members
	CAN_PROMOTE_MEMBERS       = 0x00040 # Can promote to admin
	CAN_CHANGE_INFO           = 0x00080 # Can change chat info
	CAN_INVITE_USERS          = 0x00100 # Can be a mass-adder
	CAN_PIN_MESSAGES          = 0x00200 # Can ping everyone annoyingly
	CAN_MANAGE_VOICE_CHATS    = 0x00400 # Can scream in a mic
	# Misc. 
	CAN_BE_EDITED             = 0x00800
	# Restrictions
	CAN_SEND_MESSAGES         = 0x01000
	CAN_SEND_MEDIA_MESSAGES   = 0x02000
	CAN_SEND_STICKERS         = 0x04000
	CAN_SEND_ANIMATIONS       = 0x08000
	CAN_SEND_GAMES            = 0x10000
	CAN_USE_INLINE_BOTS       = 0x20000
	CAN_ADD_WEB_PAGE_PREVIEWS = 0x40000
	CAN_SEND_POLLS            = 0x80000

	@staticmethod
	def closest_match(word: str, limit: int = 3) -> List[str]:
		if not word:
			return []

		levdist = []
		for perm in UserPermissions:
			dist = Levenshtein.distance(word, perm.name)
			levdist.append((perm.name, dist))

		levdist.sort(key=lambda tup: tup[1])
		return [x[0] for x in levdist][:limit]

	@staticmethod
	def calculate_perms(member: ChatMember) -> int:
		owo = 0

		if member.is_anonymous:              owo |= UserPermissions.CAN_USE_ANONYMOUS.value
		if member.can_be_edited:             owo |= UserPermissions.CAN_BE_EDITED.value
		if member.can_manage_chat:           owo |= UserPermissions.CAN_MANAGE_CHAT.value
		if member.can_post_messages:         owo |= UserPermissions.CAN_POST_MESSAGES.value
		if member.can_edit_messages:         owo |= UserPermissions.CAN_EDIT_MESSAGES.value
		if member.can_delete_messages:       owo |= UserPermissions.CAN_DELETE_MESSAGES.value
		if member.can_restrict_members:      owo |= UserPermissions.CAN_RESTRICT_MEMBERS.value
		if member.can_promote_members:       owo |= UserPermissions.CAN_PROMOTE_MEMBERS.value
		if member.can_change_info:           owo |= UserPermissions.CAN_CHANGE_INFO.value
		if member.can_invite_users:          owo |= UserPermissions.CAN_INVITE_USERS.value
		if member.can_pin_messages:          owo |= UserPermissions.CAN_PIN_MESSAGES.value
		if member.can_manage_voice_chats:    owo |= UserPermissions.CAN_MANAGE_VOICE_CHATS.value
		if member.can_send_messages:         owo |= UserPermissions.CAN_SEND_MESSAGES.value
		if member.can_send_media_messages:   owo |= UserPermissions.CAN_SEND_MEDIA_MESSAGES.value
		if member.can_send_stickers:         owo |= UserPermissions.CAN_SEND_STICKERS.value
		if member.can_send_animations:       owo |= UserPermissions.CAN_SEND_ANIMATIONS.value
		if member.can_send_games:            owo |= UserPermissions.CAN_SEND_GAMES.value
		if member.can_use_inline_bots:       owo |= UserPermissions.CAN_USE_INLINE_BOTS.value
		if member.can_add_web_page_previews: owo |= UserPermissions.CAN_ADD_WEB_PAGE_PREVIEWS.value
		if member.can_send_polls:            owo |= UserPermissions.CAN_SEND_POLLS.value

		return owo

	@staticmethod
	def get_flags(perms: int = 0) -> List:
		owo = []

		if perms & UserPermissions.CAN_USE_ANONYMOUS.value:         owo.append(UserPermissions.CAN_USE_ANONYMOUS)
		if perms & UserPermissions.CAN_BE_EDITED.value:             owo.append(UserPermissions.CAN_BE_EDITED)
		if perms & UserPermissions.CAN_MANAGE_CHAT.value:           owo.append(UserPermissions.CAN_MANAGE_CHAT)
		if perms & UserPermissions.CAN_POST_MESSAGES.value:         owo.append(UserPermissions.CAN_POST_MESSAGES)
		if perms & UserPermissions.CAN_EDIT_MESSAGES.value:         owo.append(UserPermissions.CAN_EDIT_MESSAGES)
		if perms & UserPermissions.CAN_DELETE_MESSAGES.value:       owo.append(UserPermissions.CAN_DELETE_MESSAGES)
		if perms & UserPermissions.CAN_RESTRICT_MEMBERS.value:      owo.append(UserPermissions.CAN_RESTRICT_MEMBERS)
		if perms & UserPermissions.CAN_PROMOTE_MEMBERS.value:       owo.append(UserPermissions.CAN_PROMOTE_MEMBERS)
		if perms & UserPermissions.CAN_CHANGE_INFO.value:           owo.append(UserPermissions.CAN_CHANGE_INFO)
		if perms & UserPermissions.CAN_INVITE_USERS.value:          owo.append(UserPermissions.CAN_INVITE_USERS)
		if perms & UserPermissions.CAN_PIN_MESSAGES.value:          owo.append(UserPermissions.CAN_PIN_MESSAGES)
		if perms & UserPermissions.CAN_MANAGE_VOICE_CHATS.value:    owo.append(UserPermissions.CAN_MANAGE_VOICE_CHATS)
		if perms & UserPermissions.CAN_SEND_MESSAGES.value:         owo.append(UserPermissions.CAN_SEND_MESSAGES)
		if perms & UserPermissions.CAN_SEND_MEDIA_MESSAGES.value:   owo.append(UserPermissions.CAN_SEND_MEDIA_MESSAGES)
		if perms & UserPermissions.CAN_SEND_STICKERS.value:         owo.append(UserPermissions.CAN_SEND_STICKERS)
		if perms & UserPermissions.CAN_SEND_ANIMATIONS.value:       owo.append(UserPermissions.CAN_SEND_ANIMATIONS)
		if perms & UserPermissions.CAN_SEND_GAMES.value:            owo.append(UserPermissions.CAN_SEND_GAMES)
		if perms & UserPermissions.CAN_USE_INLINE_BOTS.value:       owo.append(UserPermissions.CAN_USE_INLINE_BOTS)
		if perms & UserPermissions.CAN_ADD_WEB_PAGE_PREVIEWS.value: owo.append(UserPermissions.CAN_ADD_WEB_PAGE_PREVIEWS)
		if perms & UserPermissions.CAN_SEND_POLLS.value:            owo.append(UserPermissions.CAN_SEND_POLLS)

		return owo

@unique
class GroupPermissions(Flag):
	CAN_POST_MESSAGES = 0x001
	CAN_POST_PICTURES = 0x002
	CAN_POST_STICKERS = 0x004
	CAN_POST_GIFS     = 0x008
	CAN_POST_GAMES    = 0x010
	CAN_USE_INLINE    = 0x020
	CAN_EMBED_LINKS   = 0x040
	CAN_SEND_POLLS    = 0x080
	CAN_ADD_MEMBERS   = 0x100
	CAN_PIN_MESSAGES  = 0x200
	CAN_CHANGE_INFO   = 0x400

	# def get_perms(self, perms: int = 0) -> ChatPermissions:
	# 	owo = perms | self.value
	# 	return ChatPermissions(
	# 		can_send_messages         = bool(owo & GroupPermissions.CAN_POST_MESSAGES),
	# 		can_send_media_messages   = bool(owo & GroupPermissions.CAN_POST_PICTURES),
	# 		can_send_stickers         = bool(owo & GroupPermissions.CAN_POST_STICKERS),
	# 		can_send_animations       = bool(owo & GroupPermissions.CAN_POST_GIFS),
	# 		can_send_games            = bool(owo & GroupPermissions.CAN_POST_GAMES),
	# 		can_use_inline_bots       = bool(owo & GroupPermissions.CAN_USE_INLINE),
	# 		can_add_web_page_previews = bool(owo & GroupPermissions.CAN_EMBED_LINKS),
	# 		can_send_polls            = bool(owo & GroupPermissions.CAN_SEND_POLLS),
	# 		can_change_info           = bool(owo & GroupPermissions.CAN_ADD_MEMBERS),
	# 		can_invite_users          = bool(owo & GroupPermissions.CAN_PIN_MESSAGES),
	# 		can_pin_messages          = bool(owo & GroupPermissions.CAN_CHANGE_INFO),
	# 	)
		

# Mute Permissions
mute_permissions = ChatPermissions(
    can_send_messages=False,
    can_send_media_messages=False,
    can_send_stickers=False,
    can_send_animations=False,
    can_send_games=False,
    can_use_inline_bots=False,
    can_add_web_page_previews=False,
    can_send_polls=False,
    can_change_info=False,
    can_invite_users=False,
    can_pin_messages=False,
)
# Unmute permissions
unmute_permissions = ChatPermissions(
    can_send_messages=True,
    can_send_media_messages=True,
    can_send_stickers=True,
    can_send_animations=True,
    can_send_games=True,
    can_use_inline_bots=True,
    can_add_web_page_previews=True,
    can_send_polls=True,
    can_change_info=False,
    can_invite_users=True,
    can_pin_messages=False,
)

async def ResolveChatUser(owo: dict, client: Client, message: Message):
	# Attempt to resolve from the following:
	# - Mentions
	# - Replies
	# - Provided arguments

	user = None
	chat = message.chat

	# find keys, if this were C/C++ we could use if-assignment >:(
	key_chat = next((key for key,val in owo.items() if key in ['c', 'chat', 'group', 'g', 'channel']), None)
	key_user = next((key for key,val in owo.items() if key in ['u', 'user']), None)

	# if it's a reply.
	if message.reply_to_message:
		user = message.reply_to_message.from_user

	# if there's other entities here to process
	if message.entities:
		for entity in message.entities:
			if entity.type == "text_mention":
				user = entity.user
			elif entity.type == "mention":
				uwu = message.text[entity.offset:entity.offset+entity.length]
				try:
					ent, _ = await get_entity(client, uwu)
					user = ent
				except UsernameNotOccupied:
					# TODO: handle this maybe?
					pass
	if key_user:
		try:
			ent, _ = await get_entity(client, owo[key_user])
			user = ent
		except UsernameNotOccupied:
			# TODO: handle this maybe?
			pass
	if key_chat:
		try:
			ent, _ = await get_entity(client, owo[key_chat])
			chat = ent
		except UsernameNotOccupied:
			# TODO: handle this maybe?
			pass
		
	return user, chat

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['dev'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def testcmd(client, message):
	import json
	text = message.text
	user = None
	chat = None

	owo = parse_command(text)
	try:
		user, chat = await ResolveChatUser(owo, client, message)
	except Exception as ex:
		user = chat = str(ex)

	replytext = f"<b>Text:</b> <code>{text}</code>\n"
	replytext += "<b>JSON:</b> <code>" + json.dumps(owo) + "</code>\n\n"
	replytext += f"<b>User:</b> <code>{user}</code>\n<b>Chat:</b> <code>{chat}</code>"

	if len(replytext) > 4096:
		print(replytext)
		replytext = replytext[:4096]

	await message.reply(replytext)

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['perms', 'permissions'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def set_perms(client, message):
	# This command works in the following ways:
	# -perms -u=user -c=chat -p=[int|str|list] -m=calculate|apply|demote -t="admin title" -u="10 minutes"
	# where:
	# -u is the user in question (optional)
	# -c is the chat in question (required)
	# -p is the permissions either as a list or an integer, list form should look like: [+CAN_ADD_MEMBERS, +CAN_RESTRICT_MEMBERS]
	# -m is one of three options: calculate, apply, or demote.
	#    calculate - the command calculates the integer value from the provided list
	#    apply - the command actually applies the permissions, promoting the user if necessary
	#    demote - the command demotes the user entirely.
	# -t is the title for the admin (if applicable)
	# -u is the time required
	#
	# Note: If user is omitted, then the command edits the permissions of the chat itself.
	# Note: If there's an invalid permission given in the list or as a string, the command will resolve and replace with
	#       what it feels is the valid permission from the enum. It will print a warning stating the replaced permission(s).
	# Note: The permission list/string must have the prefix of + or - to denote whether the permission is to be added or removed.
	#       Omitting the permission(s) will assume them to stay in the state they were previously in or False otherwise.
	# Note: By default the command will always be in the `calculate` mode.

	user = chat = member = None
	perms: int = 0
	warnings = ""
	owo = parse_command(message.text)
	mode = next((val for key,val in owo.items() if key in ['m', 'mode']), "calculate")
	permissions = next((val for key,val in owo.items() if key in ['p', 'permissions', 'perms', 'perm', 'permission']), None)
	title = next((val for key,val in owo.items() if key in ['t', 'title']), None)
	until = next((val for key,val in owo.items() if key in ['u', 'until']), None)

	# Validate that the mode 
	mode_valid = False
	for m in ['apply', 'calculate', 'demote']:
		if m.startswith(mode.lower()):
			mode_valid = True
			break

	# Calculate the permission list.
	if permissions:
		# If the permissions is a string
		if isinstance(permissions, str):
			if permissions.isnumeric() or 'x' in permissions:
				perms = int(permissions)
			else:
				permissions = permissions.replace("+", "").replace("-", "")
				try:
					perms = UserPermissions[permissions].value
				except KeyError:
					# Attempt to resolve the closest match and issue a warning
					permission = UserPermissions.closest_match(permissions, 1)[0]
					warnings += f"Replaced <code>{permissions}</code> with <code>{permission}</code>\n"
					perms = UserPermissions[permission].value
		elif isinstance(permissions, list):
			for p in permissions:
				if p.isnumeric():
					perms |= int(p)
				else:
					# Get the prefix on whether to add or remove the permission.
					# if no prefix is given, assume it's setting the permission.
					remove = p[0] == '-'
					aaaanames = p.replace("+", "").replace("-", "")
					try:
						if remove:
							perms &= ~UserPermissions[aaaanames].value
						else:
							perms |= UserPermissions[aaaanames].value

					except KeyError:
						# Attempt to resolve the closest match and issue a warning
						permission = UserPermissions.closest_match(aaaanames, 1)[0]
						warnings += f"Replaced <code>{aaaanames}</code> with <code>{permission}</code>\n"
						if remove:
							perms &= ~UserPermissions[permission].value
						else:
							perms |= UserPermissions[permission].value

	# Handle the calculate mode where permissions are just calculated
	# or the current permissions of the user are printed.
	if "calculate".startswith(mode.lower()):
		text = "<b>Permission Calculation</b>\n"
		if not permissions: # Likely `None` so print their current perms
			user, chat = await ResolveChatUser(owo, client, message)
			if not user or not chat:
				await self_destruct(message, "<code>Cannot find the user/group</code>")
				return
			
			member = await client.get_chat_member(chat.id, user.id)
			perms = UserPermissions.calculate_perms(member)
			text = f"<b>Permissions for</b> {await build_name_flags(client, user)}\n"
		
		text += f"<b>Value:</b> <code>0x{perms:X}</code>\n\n<b>Flags:</b>\n"

		for flag in UserPermissions.get_flags(perms):
			text += f"<code>{flag.name}</code>\n"

		if warnings:
			text += "\n<code>Warnings:</code>\n" + warnings

		await message.reply(text, disable_web_page_preview=True)
		return

	if not chat.type in ["supergroup", "group"]:
		await self_destruct(message, "<code>You can only use this command in groups</code>")

	if not user or not chat:
		user, chat = await ResolveChatUser(owo, client, message)
		if not user or not chat:
			await self_destruct(message, "<code>Cannot find the user/group</code>")
			return

	if "demote".startswith(mode.lower()):
		# Just demote them, ignore the rest of the shit in the command.
		if not await client.promote_chat_member(
			chat_id=chat.id,
			user_id=user.id,
			is_anonymous=False,
			can_manage_chat=False,
			can_change_info=False,
			can_post_messages=False,
			can_edit_messages=False,
			can_delete_messages=False,
			can_restrict_members=False,
			can_invite_users=False,
			can_pin_messages=False,
			can_promote_members=False,
			can_manage_voice_chats=False
		):
			await self_destruct(message, "<code>I cannot demote that.</code>")
			return
	
		# log if we successfully demoted someone.
		chat_text = '<b>Demotion Event</b> [#DEMOTE]'
		chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
		chat_text += '\n- <b>Demoted:</b> ' + await build_name_flags(client, user)

		await log_chat(chat_text)
		await self_destruct(message, f'<a href="https://t.me/{user.username}">{user.first_name}</a><code> is no longer king.</code>')

	elif mode.lower() in "apply":
		chat_text = ""
		
		# If their perms include restrictions, apply those.
		if perms & 0xFF000:
			if not await client.restrict_chat_member(
				chat_id=chat.id,
				user_id=user.id,
				permissions=unmute_permissions
			):
				await self_destruct(message, "<code>I cannot unmute that.</code>")
				return

		else:
			if not await client.promote_chat_member(
				chat_id=chat.id,
				user_id=user.id,
				is_anonymous           = perms & UserPermissions.CAN_USE_ANONYMOUS,
				can_manage_chat        = perms & UserPermissions.CAN_MANAGE_CHAT,
				can_change_info        = perms & UserPermissions.CAN_CHANGE_INFO,
				can_post_messages      = perms & UserPermissions.CAN_POST_MESSAGES,
				can_edit_messages      = perms & UserPermissions.CAN_EDIT_MESSAGES,
				can_delete_messages    = perms & UserPermissions.CAN_DELETE_MESSAGES,
				can_restrict_members   = perms & UserPermissions.CAN_RESTRICT_MEMBERS,
				can_invite_users       = perms & UserPermissions.CAN_INVITE_USERS,
				can_pin_messages       = perms & UserPermissions.CAN_PIN_MESSAGES,
				can_promote_members    = perms & UserPermissions.CAN_PROMOTE_MEMBERS,
				can_manage_voice_chats = perms & UserPermissions.CAN_MANAGE_VOICE_CHATS
			):
				await self_destruct(message, "<code>I cannot demote that.</code>")
				return

			if title:
				if not await client.set_administrator_title(
						chat_id=chat.id,
						user_id=user.id,
						title=title
					):
						await message.edit(f'<code>I cannot set their title to "{title}"</code>')
				else:
					await message.edit(f'<code>Title set successfully to "{title}"</code>')
		
			# log if we successfully demoted someone.
			chat_text = '<b>Promotion Event</b> [#PROMOTE]'
			chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
			chat_text += '\n- <b>Demoted:</b> ' + await build_name_flags(client, user)
			if title:
				chat_text += f'\n- <b>Title:</b> <code>{title}</code>'

		await log_chat(chat_text)
		await self_destruct(message, f'<a href="https://t.me/{user.username}">{user.first_name}</a><code> is no longer king.</code>')

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['sm', 'slowmode'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def set_slowmode(client, message):
	owo = parse_command(message.text)
	_, chat = await ResolveChatUser(owo, client, message)

	if not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	# Parse the time from seconds
	time = next((val for key,val in owo.items() if key in ['t', 'time']), None)
	if not time.lower() in ["0", "0s", "none", "off", "false"]:
		time = (datetime.now()-parse(time)).seconds+1
	else:
		time = 0

	try:
		await client.set_slow_mode(chat.id, seconds=time)
	except SecondsInvalid:
		await self_destruct(message, "<code>You must specify a valid time period of: 0s (off), 10s, 30s, 60s (1m), 300s (5m), 900s (15m) or 3600s (1h)</code>")
		return

	chat_text = '<b>Slowmode Event</b> [#SLOWMODE]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
	chat_text += f'\n- <b>Time:</b> {time}s'

	await log_chat(chat_text)
	await self_destruct(message, f"<code>Slow mode now set to {time} seconds</code>")

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['promote'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def promote(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))
	they_admin = await acm.Lookup(client, chat.id, user.id)

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	if they_admin:
		await self_destruct(message, "<code>Mutany!!! I cannot allow it!</code>")
		return

	title = next((val for key,val in owo.items() if key in ['t', 'title']), None)

	if not await client.promote_chat_member(
			chat_id=chat.id,
			user_id=user.id,
			is_anonymous=False,
			can_change_info=False,
			can_manage_chat=True,
			can_post_messages=True,
			can_edit_messages=True,
			can_delete_messages=True,
			can_restrict_members=True,
			can_invite_users=True,
			can_pin_messages=True,
			can_promote_members=False,
			can_manage_voice_chats=True
		):
			await self_destruct(message, "<code>I cannot promote that.</code>")
			return

	# log if we successfully muted someone.
	chat_text = '<b>Promotion Event</b> [#PROMOTE]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
	chat_text += '\n- <b>Promoted:</b> ' + await build_name_flags(client, user)

	if title:
		try:
			if not await client.set_administrator_title(
					chat_id=chat.id,
					user_id=user.id,
					title=title
				):
					await message.edit(f'<code>User was promoted but I cannot set their title to "{title}"</code>')
			else:
				chat_text += f"\n<b>Title:</b> <code>{html.escape(title.strip()[:1000])}</code>"
		except Exception:
			await message.edit(f'<code>User was promoted but I cannot set their title to "{title}"</code>')


	await message.edit(f'<a href="https://t.me/{user.username}">{user.first_name}</a><code> can now reign too!</code>', disable_web_page_preview=True)

	await log_chat(chat_text)
	await asyncio.sleep(3)
	await message.delete()

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['t', 'title'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def title(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))
	they_admin = await acm.Lookup(client, chat.id, user.id)

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	if they_admin:
		await self_destruct(message, "<code>Mutany!!! I cannot allow it!</code>")
		return

	title = next((val for key,val in owo.items() if key in ['t', 'title']), None)

	if not await client.set_administrator_title(
			chat_id=chat.id,
			user_id=user.id,
			title=title
		):
			await message.edit(f'<code>I cannot set their title to "{title}"</code>')
	else:
		await message.edit(f'<code>Title set successfully to "{title}"</code>')
		
	await asyncio.sleep(3)
	await message.delete()
	

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['demote'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def demote(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)

	if not await client.promote_chat_member(
			chat_id=chat.id,
			user_id=user.id,
			is_anonymous=False,
			can_manage_chat=False,
			can_change_info=False,
			can_post_messages=False,
			can_edit_messages=False,
			can_delete_messages=False,
			can_restrict_members=False,
			can_invite_users=False,
			can_pin_messages=False,
			can_promote_members=False,
			can_manage_voice_chats=False
		):
			await self_destruct(message, "<code>I cannot demote that.</code>")
			return

	# log if we successfully demoted someone.
	chat_text = '<b>Demotion Event</b> [#DEMOTE]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
	chat_text += '\n- <b>Demoted:</b> ' + await build_name_flags(client, user)
	if reason.strip():
		chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

	await log_chat(chat_text)
	await self_destruct(message, f'<a href="https://t.me/{user.username}">{user.first_name}</a><code> is no longer king.</code>')


@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['m', 'mute'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def mute(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))
	they_admin = await acm.Lookup(client, chat.id, user.id)

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	if they_admin:
		await self_destruct(message, "<code>Mutany!!! I cannot allow it!</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)
	time = next((val for key,val in owo.items() if key in ['t', 'time']), None)
	uhh = None

	if time:
		uhh = parse(time)
		utcnow = calendar.timegm(datetime.utcnow().utctimetuple())
		time = calendar.timegm(uhh.utctimetuple())
		if utcnow > time:
			await self_destruct(message, "You've gotta find <a href=\"https://www.youtube.com/watch?v=v6yg4ImnYwA\">rick's cube</a> before you can do that.")
			return

	if not await client.restrict_chat_member(chat_id=chat.id, user_id=user.id, permissions=mute_permissions, until_date=time):
		await self_destruct(message, "<code>I cannot mute that.</code>")
		return

	# log if we successfully muted someone.
	chat_text = '<b>Mute Event</b> [#USERMUTE]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
	chat_text += '\n- <b>Muted:</b> ' + await build_name_flags(client, user)
	if time:
		chat_text += '\n- <b>Expires:</b> ' + uhh.strftime("hh:mm A (HH:mm) MM-DD-YYYY") + f" ({humanize.naturaldelta(uhh)})"
	if reason.strip():
		chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

	await log_chat(chat_text)
	await self_destruct(message, f'<a href="https://t.me/{user.username}">{user.first_name}</a><code>\'s enter key was removed.</code>')

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['um', 'unmute'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def unmute(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))
	they_admin = await acm.Lookup(client, chat.id, user.id)

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	if they_admin:
		await self_destruct(message, "<code>Mutany!!! I cannot allow it!</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)

	if not await client.restrict_chat_member(
			chat_id=chat.id,
			user_id=user.id,
			permissions=unmute_permissions
		):
		await self_destruct(message, "<code>I cannot unmute that.</code>")
		return

	# log if we successfully muted someone.
	chat_text = '<b>Unute Event</b> [#USERUNMUTE]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
	chat_text += '\n- <b>Unmuted:</b> ' + await build_name_flags(client, user)
	if reason.strip():
		chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

	await log_chat(chat_text)
	await self_destruct(message, f'<a href="https://t.me/{user.username}">{user.first_name}</a> <code>can now spam.</code>')

# TODO: make all the ban functions one function
# TODO: Add purge ban
# TODO: Add delete all ban

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['dban', 'deleteban'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def deleteban(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))
	they_admin = await acm.Lookup(client, chat.id, user.id)

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	if they_admin:
		await self_destruct(message, "<code>Mutany!!! I cannot allow it!</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)
	time = next((val for key,val in owo.items() if key in ['t', 'time']), None)
	uhh = None

	if time:
		uhh = parse(time)
		utcnow = calendar.timegm(datetime.utcnow().utctimetuple())
		time = calendar.timegm(uhh.utctimetuple())
		if utcnow > time:
			await self_destruct(message, "You've gotta find <a href=\"https://www.youtube.com/watch?v=v6yg4ImnYwA\">rick's cube</a> before you can do that.")
			return

	if not await client.kick_chat_member(
			chat_id=chat.id,
			user_id=user.id,
			until_date=time
		):
		await self_destruct(message, "<code>I cannot unmute that.</code>")
		return

	if message.reply_to_message:
		await client.delete_messages(message.chat.id, (message.reply_to_message.message_id, message.message_id))
	else:
		await message.delete()

	# log if we successfully kicked someone.
	chat_text = '<b>Userbot Ban Event</b> [#USERBAN]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
	chat_text += '\n- <b>Ban:</b> ' + await build_name_flags(client, user)
	if time:
		chat_text += '\n- <b>Expires:</b> ' + uhh.strftime("hh:mm A (HH:mm) MM-DD-YYYY") + f" ({humanize.naturaldelta(uhh)})"
	if reason.strip():
		chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

	await log_chat(chat_text)

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['cb', 'cban', 'commonban'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def commonban(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)
	time = next((val for key,val in owo.items() if key in ['t', 'time']), None)
	uhh = None

	if time:
		uhh = parse(time)
		utcnow = calendar.timegm(datetime.utcnow().utctimetuple())
		time = calendar.timegm(uhh.utctimetuple())
		if utcnow > time:
			await self_destruct(message, "You've gotta find <a href=\"https://www.youtube.com/watch?v=v6yg4ImnYwA\">rick's cube</a> before you can do that.")
			return

	# delete our kick command so pajeets don't try and run it themselves
	await message.delete()

	# Don't really care about chat id but do care about the entity
	me = get_id_from_app(client)
	for c in await client.get_common_chats(user.id):
		they_admin = await acm.Lookup(client, c.id, user.id)
		we_admin = await acm.Lookup(client, c.id, me)
		if they_admin or not we_admin:
			continue

		await client.kick_chat_member(
			chat_id=c.id,
			user_id=user.id,
			until_date=time
		)

		# log if we successfully kicked someone.
		chat_text = '<b>Userbot Ban Event</b> [#USERBAN]'
		chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, c)
		chat_text += '\n- <b>Ban:</b> ' + await build_name_flags(client, user)
		if reason.strip():
			chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

		await log_chat(chat_text)


@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['b', 'ban'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def banhammer(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))
	they_admin = await acm.Lookup(client, chat.id, user.id)

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	if they_admin:
		await self_destruct(message, "<code>Mutany!!! I cannot allow it!</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)
	time = next((val for key,val in owo.items() if key in ['t', 'time']), None)
	uhh = None

	if time:
		uhh = parse(time)
		utcnow = calendar.timegm(datetime.utcnow().utctimetuple())
		time = calendar.timegm(uhh.utctimetuple())
		if utcnow > time:
			await self_destruct(message, "You've gotta find <a href=\"https://www.youtube.com/watch?v=v6yg4ImnYwA\">rick's cube</a> before you can do that.")
			return

	# TODO: timed bans
	await client.kick_chat_member(
		chat_id=chat.id,
		user_id=user.id,
		until_date=time
	)

	# delete our kick command so pajeets don't try and run it themselves
	await message.delete()

	# log if we successfully muted someone.
	chat_text = '<b>Userbot Ban Event</b> [#USERBAN]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
	chat_text += '\n- <b>Ban:</b> ' + await build_name_flags(client, user)
	if time:
		chat_text += '\n- <b>Expires:</b> ' + uhh.strftime("hh:mm A (HH:mm) MM-DD-YYYY") + f" ({humanize.naturaldelta(uhh)}"
	if reason.strip():
		chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

	await log_chat(chat_text)

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['ub', 'unban'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def unbanhammer(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)
	
	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)

	if not await client.unban_chat_member(
			chat_id=chat.id,
			user_id=user.id
		):
		await self_destruct(message, "<code>I cannot unban that.</code>")
		return

	# delete our kick command so pajeets don't try and run it themselves
	await message.delete()

	# log if we successfully muted someone.
	chat_text = '<b>Userbot Unban Event</b> [#USERUNBAN]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
	chat_text += '\n- <b>Unban:</b> ' + await build_name_flags(client, user)
	if reason.strip():
		chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

	await log_chat(chat_text)


@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['add'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def add_user(client, message: Message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	# TODO: maybe support adding multiple people?
	if await client.add_chat_members(user.id, chat.id):
		await message.edit(f"<code>Successfully added to {user.title}</code>")
	else:
		await message.edit(f"<code>Failed to add {chat.title} to {user.title}</code>")
	await asyncio.sleep(3)
	await message.delete()

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & ~filters.forwarded & filters.me & filters.command(['k', 'kick'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def kick(client, message):
	owo = parse_command(message.text)
	user, chat = await ResolveChatUser(owo, client, message)

	if not user or not chat:
		await self_destruct(message, "<code>Cannot find the user/group</code>")
		return

	we_admin = await acm.Lookup(client, chat.id, get_id_from_app(client))
	they_admin = await acm.Lookup(client, chat.id, user.id)

	if not we_admin:
		await self_destruct(message, "<code>You're allown't to do that.</code>")
		return
	
	if they_admin:
		await self_destruct(message, "<code>Mutany!!! I cannot allow it!</code>")
		return

	reason = next((val for key,val in owo.items() if key in ['r', 'reason']), None)

	await client.kick_chat_member(
		chat_id=chat.id,
		user_id=user.id
	)

	# delete our kick command so pajeets don't try and run it themselves
	await message.delete()

	# Should be enough time to unban since kick == ban apparently
	await client.unban_chat_member(
		chat_id=chat.id,
		user_id=user.id
	)

	# log if we successfully muted someone.
	chat_text = '<b>Kick Event</b> [#USERKICK]'
	chat_text += '\n- <b>Chat:</b> ' + await build_name_flags(client, chat)
	chat_text += '\n- <b>Kicked:</b> ' + await build_name_flags(client, user)
	if reason.strip():
		chat_text += f'\n- <b>Reason:</b> {html.escape(reason.strip()[:1000])}'

	await log_chat(chat_text)

help_dict['moderation'] = ('Moderation',
'''{prefix}kick <i>[channel id|user id] [user id] [reason]</i> - Removes the user from the chat
Aliases: {prefix}k

{prefix}add <i>(maybe reply to a message)</i> - Adds the user to a chat (either via a reply or in the chat itself)

{prefix}promote <i>[channel id|user id] [user id] [title]</i> - Promotes the user to an administrator

{prefix}demote <i>[channel id|user id] [user id]</i> - Removes the user's administrator permissions

{prefix}title <i>[channel id|user id] [user id] [title]</i> - Sets or removes the title for the user.
Aliases: {prefix}t

{prefix}mute <i>[channel id|user id] [user id]</i> - Prevent the user from sending any messages to the chat
Aliases: {prefix}m

{prefix}unmute <i>[channel id|user id] [user id]</i> - Allow the user to send messages to the chat
Aliases: {prefix}um

{prefix}ban <i>[channel id|user id] [user id]</i> - Remove the user from the chat
Aliases: {prefix}b

{prefix}deleteban <i>[channel id|user id] [user id]</i> - Remove the user from the chat and deletes their replied to message
Aliases: {prefix}dban

{prefix}commonban <i>[channel id|user id] [user id]</i> - Remove the user from all chats in common
Aliases: {prefix}cban, {prefix}cb

{prefix}unban <i>[channel id|user id] [user id]</i> - Allow the user to participate in the chat
Aliases: {prefix}ub
''')

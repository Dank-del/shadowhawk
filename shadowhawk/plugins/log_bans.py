import asyncio
from collections import deque
from pyrogram import Client, filters
from shadowhawk import config, slave
from shadowhawk.utils import build_name_flags, name_escape
from shadowhawk.utils.Logging import log_errors, log_chat

banned = deque(maxlen=50)
lock = asyncio.Lock()

@Client.on_message(~filters.chat(config['logging']['regular']) & filters.regex(r'^[/!](?:[dgsf]*ban?)(?:$|\W+)') & filters.group)
@log_errors
async def log_ban(client, message):
	if not config['logging']['log_bans']:
		return

	# Ignored chats.
	if config['logging']['ignore_chat_bans'] and message.chat.id in config['logging']['ignore_chat_bans']:
		return

	# Ignore the slave forwards
	if not getattr(message.forward_from, 'empty', True):
		if message.forward_from.id == (await slave.get_me()).id:
			return

	# Ignore bots, they never report.
	if message.from_user:
		if message.from_user.is_bot:
			return

	identifier = (message.chat.id, message.message_id)
	async with lock:
		if identifier in banned:
			return
		text = '<b>Ban Event</b> [#BAN]\n- <b>Chat:</b> '
		text += await build_name_flags(client, message.chat)
		text += '\n- <b>Banner:</b> '
		if message.from_user:
			text += await build_name_flags(client, message.from_user)
		elif message.sender_chat and message.sender_chat.id != message.chat.id:
			text += await build_name_flags(client, message.sender_chat)
		else:
			text += 'Anonymous'
		text += f'\n- <b><a href="{message.link}">Ban Message'
		mtext = (message.text or message.caption or '').strip()
		start, end = message.matches[0].span()
		if start or end < len(mtext):
			text += ':'
		text += '</a></b>'
		if start or end < len(mtext):
			text += f' {name_escape(mtext.strip()[:1000])}'
		reply = message.reply_to_message
		if not getattr(reply, 'empty', True):
			text += '\n- <b>Bannee:</b> '
			if reply.from_user:
				text += await build_name_flags(client, reply.from_user)
			else:
				text += 'None???'
			text += f'\n- <b><a href="{reply.link}">Banned Message'
			mtext = reply.text or reply.caption or ''
			if mtext.strip():
				text += ':'
			text += f'</a></b> {name_escape(mtext.strip()[:1000])}'
			banned.append((reply.chat.id, reply.message_id))

		await log_chat(text)
		banned.append(identifier)

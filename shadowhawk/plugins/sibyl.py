import typing

from pyrogram import Client, filters
from pyrogram.types import Message

from shadowhawk import config, sibyl_client
from shadowhawk.utils import self_destruct
from shadowhawk.utils.Logging import log_errors, public_log_errors


@Client.on_message(
    ~filters.sticker & ~filters.via_bot & ~filters.edited & filters.me & filters.command(['asb', 'associationban'],
                                                                                         prefixes=config['config'][
                                                                                             'prefixes']))
@log_errors
@public_log_errors
async def ass_ban(client: Client, message: Message) -> typing.Optional[None]:
    if not sibyl_client:
        await self_destruct(message, "This feature won't work due to lack of Sibyl API connection")
        return
    cmd = message.command
    if len(cmd) == 1:
        await self_destruct(message, text="I need a chat to ban admins from")
    elif len(cmd) == 2:
        await self_destruct(message, text="Reason required")
    else:
        try:
            d = await client.get_chat_members(cmd[1], filter="administrators")
            chat = await client.get_chat(cmd[1])
            reason = " ".join(cmd[2:])
            for mem in d:
                if not mem.user.is_bot:
                    sibyl_client.add_ban(user_id=mem.user.id, reason=reason, source=message.link)
                    await message.edit_text("Banned {}".format(mem.user.mention))
            await message.edit_text("Done")
            await message.reply_text("Banned {} people for reason {}".format(len(d), reason))
        except BaseException as e:
            await self_destruct(message, "Failed due to <code>{}</code>".format(e))


@Client.on_message(
    ~filters.sticker & ~filters.via_bot & ~filters.edited & filters.me & filters.command(['asub', 'associationunban'],
                                                                                         prefixes=config['config'][
                                                                                             'prefixes']))
@log_errors
@public_log_errors
async def ass_unban(client: Client, message: Message) -> typing.Optional[None]:
    if not sibyl_client:
        await self_destruct(message, "This feature won't work due to lack of Sibyl API connection")
        return
    cmd = message.command
    if len(cmd) == 1:
        await self_destruct(message, text="I need a chat to unban admins from")
    else:
        try:
            d = await client.get_chat_members(cmd[1], filter="administrators")
            for mem in d:
                if not mem.user.is_bot:
                    sibyl_client.delete_ban(user_id=mem.user.id)
                    await message.edit_text("Banned {}".format(mem.user.mention))
            await message.edit_text("Done")
            await message.reply_text("Unbanned {} people".format(len(d)))
        except BaseException as e:
            await self_destruct(message, "Failed due to <code>{}</code>".format(e))


@Client.on_message(
    ~filters.sticker & ~filters.via_bot & ~filters.edited & filters.me & filters.command(['elm', 'eliminate'],
                                                                                         prefixes=config['config'][
                                                                                             'prefixes']))
@log_errors
@public_log_errors
async def ban_user(client: Client, message: Message) -> typing.Optional[None]:
    if not sibyl_client:
        await self_destruct(message, "This feature won't work due to lack of Sibyl API connection")
        return
    cmd = message.command
    if len(cmd) == 1:
        await self_destruct(message, text="Whom do I eliminate?")
    elif len(cmd) == 2:
        await self_destruct(message, text="Reason required")
    else:
        try:
            reason = " ".join(cmd[2:])
            d = await client.get_chat(cmd[1])
            sibyl_client.add_ban(d.id, reason=reason, source=message.link)
            await message.reply("{} was eliminated with reason {}".format((d.first_name or d.title), reason))
        except BaseException as e:
            await self_destruct(message, "Failed due to <code>{}</code>".format(e))


@Client.on_message(
    ~filters.sticker & ~filters.via_bot & ~filters.edited & filters.me & filters.command(['rev', 'revive'],
                                                                                         prefixes=config['config'][
                                                                                             'prefixes']))
@log_errors
@public_log_errors
async def unban_user(client: Client, message: Message) -> typing.Optional[None]:
    if not sibyl_client:
        await self_destruct(message, "This feature won't work due to lack of Sibyl API connection")
        return
    cmd = message.command
    if len(cmd) == 1:
        await self_destruct(message, text="Whom do I revive?")
    else:
        try:
            d = await client.get_chat(cmd[1])
            sibyl_client.delete_ban(d.id)
            await message.reply("{} was revived".format((d.first_name or d.title)))
        except BaseException as e:
            await self_destruct(message, "Failed due to <code>{}</code>".format(e))

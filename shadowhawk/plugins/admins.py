from pyrogram import Client, filters
from shadowhawk import config, help_dict, slave
from shadowhawk.utils import get_entity, self_destruct, name_escape, build_name_flags
from shadowhawk.utils.Logging import log_errors, public_log_errors

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & filters.me & filters.command(['admin', 'admins'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def admins(client, message):
	chat, entity_client = message.chat, client
	command = message.command
	command.pop(0)
	silent = False
	if '-p' in command:
		command.remove('-p')
		silent = True
	if command:
		chat = ' '.join(command)
		try:
			chat = int(chat)
		except ValueError:
			pass
		try:
			chat, entity_client = await get_entity(client, chat)
		except:
			await self_destruct(message, "<code>Invalid chat or group</code>")
			return
	text_unping = text_ping = ''
	async for i in entity_client.iter_chat_members(chat.id, filter='administrators'):
		text_unping += f'\n[<code>{i.user.id}</code>] {await build_name_flags(client, i, ping=False, include_id=False)}'
		text_ping += f'\n[<code>{i.user.id}</code>] {await build_name_flags(client, i, ping=True, include_id=False)}'
		if i.title:
			OK="\u200B"
			text_unping += f' // {name_escape(i.title.replace("@", f"@{OK}"))}'
			text_ping += f' // {name_escape(i.title)}'
	if silent:
		await slave.send_message(message.from_user.id, text_ping, disable_web_page_preview=True)
		await message.delete()
	else:
		reply = await message.reply_text(text_unping, disable_web_page_preview=True)
		if text_unping != text_ping:
			await reply.edit_text(text_ping, disable_web_page_preview=True)

helptext = '''{prefix}admins <i>[chat]</i> - Lists the admins in <i>[chat]</i>
Aliases: {prefix}admin

'''
if 'info' in help_dict:
	idk = help_dict['info']
	help_dict['info'] = (idk[0], idk[1] + helptext)
else:
	help_dict['info'] = ('Info', helptext)

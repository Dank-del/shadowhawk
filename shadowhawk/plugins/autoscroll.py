import asyncio, ormar
from pyrogram import Client, filters
from shadowhawk import config, help_dict, ee
from shadowhawk.utils import get_entity, self_destruct
from shadowhawk.utils.Logging import log_errors, public_log_errors
from shadowhawk.database import BaseModel

f = filters.chat([])
initted = False

class AutoScroll(ormar.Model):
	class Meta(BaseModel):
		tablename = 'AutoScroll'
	
	id: int =  ormar.BigInteger(primary_key=True)

@ee.on('OnDatabaseStart')
async def __init_autoscroll():
    global initted
    if not initted:
        initted = True
        chats = await AutoScroll.objects.all()
        for a in chats:
            f.add(a.id)

@Client.on_message(f)
async def auto_read(client, message):
    await client.read_history(message.chat.id)
    message.continue_propagation()

@Client.on_message(~filters.sticker & ~filters.via_bot & ~filters.edited & filters.me & filters.command(['as', 'autoscroll'], prefixes=config['config']['prefixes']))
@log_errors
@public_log_errors
async def autoscroll(client, message):
    command = message.command
    command.pop(0)
    chat = message.chat.id
    if command:
        chat = command[0]
    
    try:
        chat, entity_client = await get_entity(client, chat)
    except:
        await self_destruct("<code>Invalid chat or group</code>")
        return

    lel = await AutoScroll.objects.get_or_none(id=chat.id)

    if chat.id in f:
        f.remove(chat.id)
        if lel:
            await lel.delete()
        await message.edit(f"<code>Autoscroll disabled in {chat.title}</code>")
    else:
        f.add(chat.id)
        if not lel:
            lel = AutoScroll(id=chat.id)
            await lel.save()
        await message.edit(f"<code>Autoscroll enabled in {chat.title}</code>")
    await asyncio.sleep(3)
    await message.delete()

helptext = '''{prefix}autoscroll <i>[channel id]</i> - Automatically mark chat messages as read
Aliases: {prefix}as

'''

if 'misc' in help_dict:
	idk = help_dict['misc']
	help_dict['misc'] = (idk[0], idk[1] + helptext)
else:
	help_dict['misc'] = ('Miscellaneous', helptext)


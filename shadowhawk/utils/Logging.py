import asyncio
import functools
import traceback
import logging
import html
from pyrogram import StopPropagation, ContinuePropagation
from shadowhawk.utils.FileUtils import make_file
from shadowhawk import config, slave, apps, ee


log_ring = asyncio.Queue(maxsize=config['logging']['regular_ring_size'])
spammy_log_ring = asyncio.Queue(maxsize=config['logging']['spammy_ring_size'])

def log_errors(func):
	@functools.wraps(func)
	async def wrapper(client, *args):
		try:
			await func(client, *args)
		except (StopPropagation, ContinuePropagation):
			raise
		except Exception:
			tb = traceback.format_exc()
			is_vomit =  len(tb) >= 4096
			try:
				if is_vomit:
					await slave.send_document(config['logging']['regular'], make_file(tb.strip()), caption=f"Exception occured in <code>{func.__name__}</code>")
				else:
					await slave.send_message(config['logging']['regular'], f'Exception occured in <code>{func.__name__}\n\n{html.escape(tb)}</code>', disable_web_page_preview=True)
			except Exception:
				logging.exception('Failed to log exception for %s as slave', func.__name__)
				tb2 = traceback.format_exc()
				is_vomit_again = len(tb2) >= 4096
				for app in apps.values():
					try:
						if is_vomit:
							puke_msg = await app.send_document(config['logging']['regular'], make_file(tb.strip()), caption=f"Exception occured in <code>{func.__name__}</code>")
							if is_vomit_again:
								await puke_msg.reply_document(make_file(tb2.strip()), caption="Exception occured in the exception handler")
							else:
								await puke_msg.reply(f'Exception occured in exception handler <code>{func.__name__}\n\n{html.escape(tb2)}</code>', disable_web_page_preview=True)
						else:
							puke_msg = await app.send_message(config['logging']['regular'], f'Exception occured in <code>{func.__name__}\n\n{html.escape(tb)}</code>', disable_web_page_preview=True)
							if is_vomit_again:
								await puke_msg.reply_document(make_file(tb2.strip()), caption="Exception occured in the exception handler")
							else:
								await puke_msg.reply(f'Exception occured in exception handler <code>{func.__name__}\n\n{html.escape(tb2)}</code>', disable_web_page_preview=True)
					except Exception:
						logging.exception('Failed to log exception for %s as app', func.__name__)
						tb = traceback.format_exc()
					else:
						break
				raise
			raise
	return wrapper

def public_log_errors(func):
	@functools.wraps(func)
	async def wrapper(client, message):
		try:
			await func(client, message)
		except (StopPropagation, ContinuePropagation):
			raise
		except Exception as ex:
			await message.reply_text("<code>" + html.escape(f"{type(ex).__name__}: {str(ex)}") + "</code>", disable_web_page_preview=True)
			raise
	return wrapper

async def log_chat(message, chat="regular"):
	# Append it to the queue
	ee.emit('OnLogRingInsert', message, chat)
	if chat == 'regular':
		await log_ring.put((message, chat))
	else:
		try:
			spammy_log_ring.put_nowait((message, chat))
		except asyncio.QueueFull:
			pass
import asyncio
import html
from pyrogram import Client
from pyrogram.types import User, Chat, Message, ChatMember
from pyrogram.errors.exceptions.bad_request_400 import PeerIdInvalid, ChannelInvalid
from shadowhawk import slave, apps

def name_escape(name):
	if name:
		return "\u200E" + html.escape(name) + "\u200E"
	else:
		return None

async def build_name_flags(client, entity, ping=True, include_id=True):
	text = getattr(entity, 'title', None)
	ent = entity
	if isinstance(entity, ChatMember):
		ent = entity.user

	if not text:
		text = ent.first_name
		if ent.last_name:
			text += f' {ent.last_name}'
	sexy_text = '<code>[DELETED]</code>' if getattr(entity, 'is_deleted', False) else name_escape(text or 'Empty???')
	if not getattr(ent, 'is_deleted', False):
		if ping and getattr(ent, 'type', None) and ent.type in ('private', 'bot') and text:
			sexy_text = f'<a href="tg://user?id={ent.id}">{sexy_text}</a>'
		elif ent.username:
			sexy_text = f'<a href="https://t.me/{ent.username}">{sexy_text}</a>'

		if not ping:
			sexy_text = sexy_text.replace('@', '@\u200B')
	if getattr(ent, 'type', None) == 'bot':
		sexy_text += ' <code>[BOT]</code>'
	if getattr(ent, 'is_verified', False):
		sexy_text += ' <code>[VERIFIED]</code>'
	if getattr(ent, 'is_support', False):
		sexy_text += ' <code>[SUPPORT]</code>'
	if getattr(ent, 'is_scam', False):
		sexy_text += ' <code>[SCAM]</code>'
	if getattr(ent, 'is_fake', False):
		sexy_text += ' <code>[FAKE]</code>'

	if isinstance(entity, ChatMember):
		if getattr(entity, 'is_anonymous', False):
			sexy_text += ' <code>[ANON]</code>'
		if getattr(entity, 'status', "") == "creator":
			sexy_text += ' <code>[CREATOR]</code>'

	if include_id:
		sexy_text += f' [<code>{ent.id}</code>]'

	return sexy_text


async def get_entity(client, entity):
	entity_client = client
	if not isinstance(entity, Chat):
		try:
			entity = int(entity)
		except ValueError:
			pass
		except TypeError:
			entity = entity.id
		try:
			entity = await client.get_chat(entity)
		except (PeerIdInvalid, ChannelInvalid):
			for app_id, app in apps.items():
				if app != client:
					try:
						entity = await app.get_chat(entity)
					except (PeerIdInvalid, ChannelInvalid):
						pass
					else:
						entity_client = app
						break
			else:
				entity = await slave.get_chat(entity)
				entity_client = slave
	return entity, entity_client

async def get_user(client, entity):
	entity_client = client
	if not isinstance(entity, User):
		try:
			entity = int(entity)
		except ValueError:
			pass
		except TypeError:
			entity = entity.id
		try:
			entity = await client.get_users(entity)
		except PeerIdInvalid:
			for app in apps:
				if app != client:
					try:
						entity = await app.get_users(entity)
					except PeerIdInvalid:
						pass
					else:
						entity_client = app
						break
			else:
				entity = await slave.get_users(entity)
				entity_client = slave
	return entity, entity_client

async def get_app(id):
	for saved_id, app in apps.items():
		if saved_id == id:
			return app
	return None

def get_id_from_app(app: Client):
	return next((key for key,val in apps.items() if val == app), None)

async def get_chat_link(client, message):
	if not isinstance(message, Message):
		raise PeerIdInvalid

	if message.chat.username:
		return f"https://t.me/{message.chat.username}/{message.message_id}"
	elif getattr(message, 'link', None):
		return message.link
	else:
		# Last resort.
		peer_id = (await client.resolve_peer(message.chat.id)).channel_id
		return f"https://t.me/c/{peer_id}/{message.message_id}"
	return ""

async def self_destruct(message, text):
	await message.edit(text, disable_web_page_preview=True)
	await asyncio.sleep(3)
	await message.delete()
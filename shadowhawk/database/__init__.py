from sqlalchemy import Column, BigInteger, MetaData
from sqlalchemy import create_engine
from sqlalchemy import ext
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import  sessionmaker
from shadowhawk import ObjectProxy as SessionProxy
from shadowhawk import config, ee

import ormar, databases

Base = declarative_base()

session = SessionProxy()
session_factory = SessionProxy()

# Hacked fuckery.
bits =  config['sql']['uri'].split("+", 1)
wtfuri = f'{bits[0]}:{bits[1].split(":", 1)[1]}'

# Start up ormar
database = databases.Database(wtfuri, min_size=1, max_size=config['sql']['poolsize'])
metadata = MetaData()

# Ormar base model 
class BaseModel(ormar.ModelMeta):

	# Set the Ormar stuff
	metadata = metadata
	database = database

class AutoBanSpammers(Base):
	__tablename__ = 'AutoBanSpammers'
	id = Column(BigInteger, primary_key=True)

	def __init__(self, id):
		self.id = id

	def __repr__(self):
		return f"<AutoBanSpammers {self.id}>"

# we're british I guess
async def innit():
	# Initialize SQL

	# Start up sql alchemy
	sqlengine = create_async_engine(config['sql']['uri'], pool_size=config['sql']['poolsize'], echo=config['sql']['debug'])
	ormar_sqlengine = create_engine(config['sql']['uri'].replace("+asyncpg", ""), pool_size=config['sql']['poolsize'], echo=config['sql']['debug'])
	Base.metadata.bind = sqlengine

	# Connect?
	if not database.is_connected:
		await database.connect()

	# ormar?
	metadata.create_all(ormar_sqlengine)

	# Create the tables if they don't already exist
	try:
		async with sqlengine.begin() as conn:
			await conn.run_sync(Base.metadata.create_all)
			await conn.commit()
	except ext.OperationalError:
		return False

	# TODO: find a way to get scoped_session to work?
	session_maker = sessionmaker(sqlengine, autoflush=False, future=True, class_=AsyncSession)
	session_factory.set_thing(session_maker)
	sesh = session_maker()
	session.set_thing(sesh)

	ee.emit('OnDatabaseStart')
	return True

